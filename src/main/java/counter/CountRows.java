package counter;

import org.apache.spark.sql.Dataset;
import org.apache.spark.sql.Row;
import org.apache.spark.sql.SparkSession;
public class CountRows {
    public static void main(String[] args) {
        // Ініціалізація SparkSession
        SparkSession spark = SparkSession.builder()
                .appName("Count Rows")
                .master("spark://spark-master:7077")
                .config("spark.jars.packages", "org.apache.hadoop:hadoop-aws:2.7.4") // Додано залежність для підтримки роботи з Hadoop
                .getOrCreate();

        // Завантаження CSV файлу
        Dataset<Row> df = spark.read()
                .option("header", "true")
                .csv("counter/test.csv");

        // Підрахунок кількості рядків
        long rowCount = df.count();

        // Вивід кількості рядків на екран
        System.out.println("Number of rows: " + rowCount);

        // Зупинка SparkSession
        spark.stop();
    }
}